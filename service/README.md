# Raken Communications Service 

## Outline

The Raken Communications Service is responsible for the delivery of Notifications to customers via email and SMS

### Related Information

- [Solution Summary](https://rakenapp.atlassian.net/wiki/spaces/DEV/pages/557318337/Communication+Microservice+Phase+1+Solution+Summary)
- [Continuous Integration](http://jenkins-ng.rakenapp.com/blue/organizations/jenkins/Raken%20Bitbucket%2Fraken-notification/detail/master/1/pipeline)

### API Docs

- Swagger UI available [here](http://comms.dev.rakenapp.com/swagger-ui.html)
- Swagger spec available [here](http://comms.dev.rakenapp.com/v2/api-docs?group=Raken-Communications-Api)

### Admin

- Admin API spec available [here](http://comms.dev.rakenapp.com/admin)
- Metrics available [here](http://comms.dev.rakenapp.com/admin/metrics)
- Build information available [here](http://comms.dev.rakenapp.com/admin/info)
- Health check available [here](http://comms.dev.rakenapp.com/admin/health)
- Environment configuration available [here](http://comms.dev.rakenapp.com/admin/env)
- Logging configuration available [here](http://comms.dev.rakenapp.com/admin/loggers)

### Running Locally

#### Within IDE 

- Ensure that you have the Lombok plugin installed 
- Ensure that you have setup your AWS credentials in your local ~/.aws/credentials file i.e. `less ~/.aws/credentials`
- Run [RakenCommunicationApplication](src/main/java/com/raken/comms/RakenCommunicationApplication.java) 

### From Gradle: 

```
gradle bootRun
```

### Building Docker Image

```
./gradlew clean build docker
```

### Pushing Docker Image to Nexus

```
./gradlew clean dockerPush
```

### Twilio

- https://www.twilio.com/blog/2018/04/twilio-test-credentials-magic-numbers.html