package com.raken.comms.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class UrlUtils {

    private static final String UTF_8 = "UTF-8";

    /**
     * ${baseUrl}${path}${signature}?email=${encodedEmail}
     * @param rakenappBaseUrl The base URL to raken app (app.rakenapp.com)
     * @param signature The HMAC signature
     * @param email The users email
     * @return An unsubscribe link based on the input provided.
     */
    public static String makeUnsubscribeLink(String rakenappBaseUrl, String signature, String email) {

        if ( isNotBlank(email) && isNotBlank(signature) ) {

            final String path = "/email/preferences/";

            try {
                final String encodedEmail = URLEncoder.encode(email, UTF_8);
                return rakenappBaseUrl + path + signature + "?email=" + encodedEmail;
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(format("Cannot encode email %s", email));
            }
        }

        throw new RuntimeException(
                format("Cannot make unsubscribe url for email=%s, signature=%s, rakenappBaseUrl=%s", email, signature, rakenappBaseUrl)
        );
    }
}
