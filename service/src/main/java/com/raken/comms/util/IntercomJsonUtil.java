package com.raken.comms.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

import static io.intercom.api.MapperSupport.objectMapper;

public class IntercomJsonUtil {

    public static <T> T jsonPatch(T input, String patch){
        if (input == null) {
            return null;
        }
        try {

            return (T) convert(
                    merge(convert(input, JsonNode.class), readTree(patch)),
                    input.getClass()
            );

        } catch(IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    private static JsonNode readTree(String patch) throws IOException {
        return objectMapper().readTree(patch);
    }

    private static JsonNode merge(final JsonNode docNode, final JsonNode patchNode) {

        patchNode.fieldNames().forEachRemaining(patchFieldName -> {

            final JsonNode documentFieldNode = docNode.get(patchFieldName);
            final JsonNode patchFieldNode = getPatchFieldNode(patchNode, patchFieldName);

            if (isArrayNode(documentFieldNode) && patchFieldNode.isArray()) {
                mergeArray((ArrayNode) documentFieldNode, (ArrayNode) patchFieldNode);
            } else if(patchFieldNode.isNull() && documentFieldNode != null) {
                ((ObjectNode) docNode).replace(patchFieldName, null);
            } else if (documentFieldNode != null && documentFieldNode.isObject()) {
                merge(documentFieldNode, patchFieldNode);
            } else if(docNode instanceof ObjectNode) {
                ((ObjectNode) docNode).replace(patchFieldName, patchFieldNode);
            }
        });

        return docNode;
    }

    private static JsonNode getPatchFieldNode(final JsonNode patchNode, final String patchFieldName) {

        JsonNode patchFieldNode = patchNode.get(patchFieldName);

        if(patchFieldNode.isTextual()) {
            final TextNode textNode = (TextNode) patchFieldNode;
            if(StringUtils.isBlank(textNode.asText())) {
                return new TextNode(null);
            }
            final String trimmed = textNode.asText().trim();
            if (!trimmed.equals(textNode.asText())) {
                return new TextNode(trimmed);
            }
        }
        return patchFieldNode;
    }

    private static boolean isArrayNode(final JsonNode valueToBeUpdated) {
        return valueToBeUpdated != null && valueToBeUpdated.isArray();
    }

    private static void mergeArray(final ArrayNode valueToBeUpdated, final ArrayNode updatedValue) {
        valueToBeUpdated.removeAll();
        valueToBeUpdated.addAll(updatedValue);
    }

    public static <T> T fromJson(String chatEventJson, Class<T> clazz) {
        try {
            return objectMapper().readValue(chatEventJson, clazz);
        } catch (IOException e) {
            throw new RuntimeException("chat event not parsable", e);
        }
    }

    public static <T> T fromJson(String chatEventJson, TypeReference<T> typeRef) {
        try {
            return objectMapper().readValue(chatEventJson, typeRef);
        } catch (IOException e) {
            throw new RuntimeException("chat event not parsable", e);
        }
    }

    public static String toJson(Object obj){
        try {
            return objectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> T convert(Object obj, Class<T> clazz){
        return objectMapper().convertValue(obj, clazz);
    }

}
