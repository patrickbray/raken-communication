package com.raken.comms.util;

import org.apache.commons.codec.binary.Hex;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class HashUtils {

    private static final String CHARSET_NAME = "UTF-8";
    private static final String ALGORITHM = "HmacSHA1";

    public static Mac getHmac(@NotNull String secret) {

        if( isBlank(secret) ) {
            throw new RuntimeException("Cannot create HMAC instance. Secret is empty.");
        }

        try {
            final Mac sha1HMAC = Mac.getInstance(ALGORITHM);
            final SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes(CHARSET_NAME), ALGORITHM);
            sha1HMAC.init(secretKey);
            return sha1HMAC;
        } catch (Exception e) {
            throw new RuntimeException("Cannot create HMAC instance.");
        }
    }

    public static String calculateHmacHash(Mac hmac, String message) {
        try {
            return Hex.encodeHexString(hmac.doFinal(message.getBytes(CHARSET_NAME)));
        } catch (Exception e) {
            throw new RuntimeException("Cannot check HMAC hash.");
        }
    }
}
