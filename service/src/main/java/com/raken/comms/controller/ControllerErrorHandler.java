package com.raken.comms.controller;

import com.raken.comms.model.ErrorResponse;
import com.raken.comms.model.ValidationDetail;
import com.raken.comms.exception.ValidationException;
import com.raken.comms.service.RakenTracer;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.annotation.Resource;
import java.util.List;

import static java.util.stream.Collectors.toList;

@ControllerAdvice
public class ControllerErrorHandler {

    private static final Logger log = LoggerFactory.getLogger(ControllerErrorHandler.class);

    @Resource
    private RakenTracer rakenTracer;

    @InitBinder
    private void activateDirectFieldAccess(DataBinder dataBinder) {
        dataBinder.initDirectFieldAccess();
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody ErrorResponse validationExceptionHandler(ValidationException exception) {
        return new ErrorResponse(exception.getMessage(), HttpStatus.BAD_REQUEST.value(), getTraceId(), exception.getValidationDetails());
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ErrorResponse unhandledExceptionHandler(Throwable throwable) {
        final int errorCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        log.error("INTERNAL SERVER ERROR", throwable);
        return new ErrorResponse(throwable.getMessage(), errorCode, getTraceId());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody ErrorResponse methodArgumentNotValidExceptionHandler(final MethodArgumentNotValidException exception) {
        final BindingResult result = exception.getBindingResult();
        final List<FieldError> errors = result.getFieldErrors();
        return new ErrorResponse("Validation Failed",
                HttpStatus.BAD_REQUEST.value(),
                getTraceId(),
                errors.stream()
                        .map(error -> new ValidationDetail(error.getField(), error.getDefaultMessage()))
                        .collect(toList()));
    }

    @Nullable
    private String getTraceId() {
        return rakenTracer.getTraceIdString();
    }
}
