package com.raken.comms.controller;

import com.raken.comms.constant.ApiTagConstants;
import com.raken.comms.model.EmailDeliveryRequest;
import com.raken.comms.service.EmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping("/email")
@Api(tags = ApiTagConstants.EMAIL, description = "Email APIs", produces = MediaType.APPLICATION_JSON_VALUE)
public class EmailController {

    private static final Logger log = LoggerFactory.getLogger(EmailController.class);

    @Resource
    private EmailService emailService;

    @ApiResponses(
            {
                    @ApiResponse(code = 202, message = "Email queued for delivery"),
                    @ApiResponse(code = 200, message = "Email sent")
            }
    )
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Send email", nickname = "sendEmail", tags = ApiTagConstants.EMAIL)
    public ResponseEntity<Void> sendEmail(@Valid @RequestBody EmailDeliveryRequest emailDeliveryRequest,
                                          @RequestParam(value = "sync", defaultValue = "false") boolean sync) {

        log.debug("Received request to send EMAIL={}, sync={}", emailDeliveryRequest, sync);

        if (sync) {
            emailService.sendEmail(emailDeliveryRequest);
            return ResponseEntity.ok().build();
        } else {
            emailService.queueEmail(emailDeliveryRequest);
            return ResponseEntity.accepted().build();
        }
    }
}
