package com.raken.comms.controller;


import com.raken.comms.constant.ApiTagConstants;
import com.raken.comms.model.HelpChatEvent;
import com.raken.comms.model.HelpChatEventType;
import com.raken.comms.service.HelpChatService;
import io.intercom.api.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/helpchat")
@Api(tags = ApiTagConstants.HELPCHAT, description = "Customer messaging APIs")
public class HelpChatController {

    private static final Logger log = LoggerFactory.getLogger(HelpChatController.class);

    @Resource
    private HelpChatService helpChatService;

    @ApiResponses(
            {
                    @ApiResponse(code = 202, message = "Help Chat sync request queued for delivery"),
            }
    )
    @PatchMapping("/sync")
    @ApiOperation(value = "sync help chat data", nickname = "syncHelpChatData", tags = ApiTagConstants.HELPCHAT)
    public ResponseEntity<Void> syncHelpChatData(@RequestBody HelpChatEvent helpChatEvent) {

        helpChatService.queueEvent(helpChatEvent);

        return ResponseEntity.accepted().build();
    }



}
