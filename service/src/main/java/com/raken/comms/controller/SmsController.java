package com.raken.comms.controller;

import com.raken.comms.constant.ApiTagConstants;
import com.raken.comms.model.SmsDeliveryRequest;
import com.raken.comms.service.SmsService;
import com.raken.comms.service.TwilioClient;
import com.raken.comms.validation.PhoneNumber;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(ApiTagConstants.SMS)
@Api(tags = ApiTagConstants.SMS, description = "SMS APIs")
public class SmsController {

    private static final Logger log = LoggerFactory.getLogger(SmsController.class);

    @Resource
    private SmsService smsService;

    @Resource
    private TwilioClient twilioClient;

    @ApiResponses(
            {
                    @ApiResponse(code = 202, message = "SMS queued for delivery"),
                    @ApiResponse(code = 200, message = "SMS sent")
            }
    )
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Send SMS", nickname = "sendSms", tags = ApiTagConstants.SMS)
    public ResponseEntity<Void> sendSms(@Valid @RequestBody SmsDeliveryRequest smsDeliveryRequest,
                                        @RequestParam(value = "sync", defaultValue = "false") boolean sync) {

        log.debug("Received request to send SMS={}, sync={}", smsDeliveryRequest, sync);

        if (sync) {
            smsService.sendSms(smsDeliveryRequest);
            return ResponseEntity.ok().build();
        } else {
            smsService.queueSms(smsDeliveryRequest);
            return ResponseEntity.accepted().build();
        }
    }

    @PostMapping(value = "lookup", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Lookup Number", nickname = "lookupNumber", tags = ApiTagConstants.SMS)
    public ResponseEntity<com.twilio.rest.lookups.v1.PhoneNumber> validateNumber(@RequestBody @Valid @PhoneNumber String phoneNumber) {

        final Optional<com.twilio.rest.lookups.v1.PhoneNumber> validatedPhoneNumber =
                twilioClient.validateNumber(phoneNumber);

        return validatedPhoneNumber.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}