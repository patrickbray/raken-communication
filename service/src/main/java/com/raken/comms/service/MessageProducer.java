package com.raken.comms.service;

public interface MessageProducer {

    void queueMessage(final String json);

    String getQueueUrl();

    String getQueueName();
}
