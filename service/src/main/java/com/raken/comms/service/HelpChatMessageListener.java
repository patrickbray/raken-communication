package com.raken.comms.service;

import brave.ScopedSpan;
import brave.Span;
import brave.Tracer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.raken.comms.model.ConvertEventData;
import com.raken.comms.model.HelpChatEvent;
import io.intercom.api.Company;
import io.intercom.api.Event;
import io.intercom.api.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

import static com.raken.comms.service.FifoSqsProducer.SPAN_ID_NAME;
import static com.raken.comms.service.FifoSqsProducer.TRACE_ID_NAME;
import static com.raken.comms.util.IntercomJsonUtil.fromJson;

@Component
public class HelpChatMessageListener {

    private final static Logger logger = LoggerFactory.getLogger(HelpChatMessageListener.class);

    @Resource
    private RakenTracer rakenTracer;

    @Resource
    private Tracer tracer;

    @Resource
    private HelpChatService helpChatService;

    @JmsListener(destination = "${helpchat.queue.name}", containerFactory = "helpChatListenerContainerFactory")
    public void receiveChatEventRequest(final String requestJSON,
                                        @Header(value = TRACE_ID_NAME, required = false) Long traceId,
                                        @Header(value = SPAN_ID_NAME, required = false) Long spanId){

        final Span parentSpan = rakenTracer.createSpanFromIds(traceId, spanId);
        final ScopedSpan receiveHelpChatEvent = tracer.startScopedSpanWithParent("receive-helpchat-request", parentSpan.context());

        try {

            logger.debug("Received message={}", requestJSON);

            HelpChatEvent helpChatEvent = fromJson(requestJSON, HelpChatEvent.class);

            switch (helpChatEvent.getType()) {
                case SYNC_USER: {
                    helpChatService.syncUser(
                            helpChatEvent.getIdentifier(),
                            fromJson(helpChatEvent.getData(), User.class),
                            helpChatEvent.getOptions().getIgnoreOnConflictAttrs(),
                            helpChatEvent.getOptions().getCreateOnNotFound()
                    );
                    break;
                }
                case SYNC_COMPANY:{
                    helpChatService.syncCompany(
                            fromJson(helpChatEvent.getData(), Company.class),
                            helpChatEvent.getOptions().getCreateOnNotFound()
                    );
                    break;
                }
                case SYNC_LEAD_BY_EMAIL:{
                    helpChatService.syncLead(
                            helpChatEvent.getIdentifier(),
                            helpChatEvent.getData(),
                            helpChatEvent.getOptions().getCreateOnNotFound()
                    );
                    break;
                }
                case SYNC_LEAD: {
                    helpChatService.syncLead(
                            helpChatEvent.getData(),
                            helpChatEvent.getOptions().getCreateOnNotFound()
                    );
                    break;
                }
                case CONVERT_LEAD: {
                    ConvertEventData convertEventData = fromJson(helpChatEvent.getData(), ConvertEventData.class);
                    helpChatService.convert(
                            convertEventData.getLeadIdentifier(),
                            convertEventData.getUserEmail()
                    );
                    break;
                }
                case SYNC_EVENT: {
                    helpChatService.syncEvent(
                            fromJson(helpChatEvent.getData(), Event.class)
                    );
                    break;
                }
                case BULK_DELETE_USERS: {
                    helpChatService.bulkDeleteUser(
                            fromJson(helpChatEvent.getData(), new TypeReference<List<String>>() {})
                    );
                    break;
                }
            }


        } finally {
            receiveHelpChatEvent.finish();
        }

    }
}
