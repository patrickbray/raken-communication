package com.raken.comms.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.raken.comms.model.SmsDeliveryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SmsService {

    private static final Logger log = LoggerFactory.getLogger(SmsService.class);

    @Resource
    private MessageProducer smsQueueProducer;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private TwilioClient twilioClient;

    @NewSpan(name = "queueSms")
    public void queueSms(final SmsDeliveryRequest smsDeliveryRequest) {

        try {
            final String json = objectMapper.writeValueAsString(smsDeliveryRequest);
            smsQueueProducer.queueMessage(json);
        } catch (JsonProcessingException e) {
            log.error("Error serialising to json", e);
            throw new RuntimeException("Error serialising to json", e);
        }
    }

    @NewSpan(name = "sendSms")
    public void sendSms(final SmsDeliveryRequest smsDeliveryRequest) {
        log.debug("Sending sms {}", smsDeliveryRequest);
        twilioClient.sendSms(smsDeliveryRequest);
    }
}
