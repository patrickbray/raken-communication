package com.raken.comms.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.raken.comms.model.EmailDeliveryRequest;
import com.sendgrid.ASM;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Personalization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;

@Service
public class EmailService {

    private static final Logger log = LoggerFactory.getLogger(EmailService.class);

    @Value("${mail.from:no-reply@rakenapp.com}")
    private String mailFrom;

    @Value("${unsubscribe.transactional.emails}")
    private int unsubscribeTransactionalEmailsGroup;

    @Resource
    private SendGridClient sendGridClient;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private MessageProducer emailQueueProducer;

    @Resource
    private SendGridContactManagementService sendGridContactManagementService;

    @NewSpan("sendEmail")
    public void sendEmail(final EmailDeliveryRequest emailDeliveryRequest) {

        final Mail mail = new Mail();
        mail.setFrom(new Email(mailFrom));
        mail.addContent(new Content(MediaType.TEXT_HTML_VALUE, emailDeliveryRequest.body));
        mail.setSubject(emailDeliveryRequest.subject);

        final Personalization personalization = new Personalization();

        emailDeliveryRequest.to.stream()
                .map(Email::new)
                .forEach(personalization::addTo);

        if( isNotEmpty(emailDeliveryRequest.cc) ) {
            emailDeliveryRequest.cc.stream()
                    .map(Email::new)
                    .forEach(personalization::addCc);
        }

        if( isNotEmpty(emailDeliveryRequest.bcc) ) {
            emailDeliveryRequest.bcc.stream()
                    .map(Email::new)
                    .forEach(personalization::addBcc);
        }

        mail.addPersonalization(personalization);

        if( isNotEmpty(emailDeliveryRequest.categories) ) {
            emailDeliveryRequest.categories.forEach(mail::addCategory);
        }

        if ( emailDeliveryRequest.unsubscribeGroup != null) {
            mail.setASM(createASM(emailDeliveryRequest.unsubscribeGroup));
        } else {
            mail.setASM(createASM(unsubscribeTransactionalEmailsGroup));
        }

        sendGridClient.sendEmail(mail);

        sendGridContactManagementService.updateContactsUnsubscribeLinks(mail);
    }

    private ASM createASM(int groupId) {
        final ASM asm = new ASM();
        asm.setGroupId(groupId);
        return asm;
    }

    @NewSpan("queueEmail")
    public void queueEmail(EmailDeliveryRequest emailDeliveryRequest) {

        try {
            final String json = objectMapper.writeValueAsString(emailDeliveryRequest);
            emailQueueProducer.queueMessage(json);
        } catch (JsonProcessingException e) {
            log.error("Error serialising to json", e);
            throw new RuntimeException("Error serialising to json", e);
        }
    }
}

