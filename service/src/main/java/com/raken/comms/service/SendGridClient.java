package com.raken.comms.service;

import com.raken.comms.constant.TraceTags;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.MailSettings;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.Setting;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.raken.comms.service.SendGridClient.SendGridHeaders.X_MESSAGE_ID;
import static org.apache.commons.collections4.MapUtils.isNotEmpty;

@Component
public class SendGridClient {

    private static final Logger log = LoggerFactory.getLogger(SendGridClient.class);

    interface SendGridHeaders {
        String X_MESSAGE_ID = "X-Message-Id";
    }

    @Value("${email.whitelist:.+@rakenapp.com|.+@neklo.com}")
    private String emailWhiteList;

    @Value("${sendgrid.sandbox.mode:false}")
    private boolean sandboxMode;

    private Pattern emailWhitelistPattern;

    @Resource
    private SendGrid sendGrid;

    @Resource
    private RakenTracer rakenTracer;

    @PostConstruct
    public void postConstruct() {
         this.emailWhitelistPattern = Pattern.compile(emailWhiteList);
    }

    @NewSpan("sendgrid-mail-send")
    public void sendEmail(final Mail mail) {

        if (sandboxMode && ! matchesWhiteList(mail) ) {
            log.warn("Sendgrid sandbox mode enabled email will not be sent");
            rakenTracer.addTag(TraceTags.SENDGRID_SAND_BOXED, "true");
            mail.setMailSettings(getSandBoxedMailSettings());
        }

        sendMail(mail);
    }

    void sendMail(Mail mail) {

        final Request request = new Request();

        try {

            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());

            final Response response = sendGrid.api(request);

            if ( isNotEmpty(response.getHeaders()) &&
                    response.getHeaders().containsKey(X_MESSAGE_ID)) {

                rakenTracer.addTag(TraceTags.SENDGRID_MESSAGE_ID, response.getHeaders().get(X_MESSAGE_ID));
            }

            log.debug("SendGrid Response: statusCode={} body={} headers={}",
                    response.getStatusCode(),
                    response.getBody(),
                    response.getHeaders());

            rakenTracer.addTag(TraceTags.SENDGRID_STATUS_CODE, response.getStatusCode());

        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean matchesWhiteList(final Mail mail) {
        return mail.getPersonalization()
                .stream()
                .allMatch(this::personalisationMatchesWhitelist);
    }

    private boolean personalisationMatchesWhitelist(final Personalization personalization) {
        return Stream.of(personalization.getTos(), personalization.getCcs(), personalization.getBccs())
                .allMatch(this::emailListMatchesWhiteList);
    }

    private boolean emailListMatchesWhiteList(final List<Email> emails) {
        return emails.stream()
                .allMatch(email -> emailWhitelistPattern.matcher(email.getEmail()).matches());
    }

    @NotNull
    private MailSettings getSandBoxedMailSettings() {
        final MailSettings mailSettings = new MailSettings();
        mailSettings.setSandboxMode(enabled());
        return mailSettings;
    }

    @NotNull
    private Setting enabled() {
        final Setting setting = new Setting();
        setting.setEnable(true);
        return setting;
    }

}
