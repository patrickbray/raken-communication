package com.raken.comms.service;

import com.google.common.collect.ImmutableMap;
import com.raken.comms.constant.TraceTags;
import com.raken.comms.model.SmsDeliveryRequest;
import com.raken.comms.exception.UndeliverableSmsException;
import com.twilio.exception.ApiException;
import com.twilio.http.TwilioRestClient;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.lookups.v1.PhoneNumber;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static net.logstash.logback.marker.Markers.appendEntries;

@Component
public class TwilioClient {

    private static final Logger log = LoggerFactory.getLogger(TwilioClient.class);

    /**
     * See here for more details on <a href="https://www.twilio.com/docs/api/errors">Twilio Errors</a>
     */
    interface TwilioErrorCodes {
        Integer INVALID_TO = 21211;
        Integer INVALID_TO_NON_MOBILE = 21614;
        Integer CUSTOMER_UNSUBSCRIBED = 21610;
    }

    interface CustomMetrics {
        String TWILIO_SMS_SUCCESS = "twilio.sms.success";
        String TWILIO_SMS_ERROR = "twilio.sms.error";
    }

    private static final List<Integer> INVALID_TO_NUMBER_ERROR_CODES =
            asList(TwilioErrorCodes.INVALID_TO, TwilioErrorCodes.INVALID_TO_NON_MOBILE);

    private Counter twilioSuccess;
    private Counter twilioError;

    @Value("${sms.default.from:+16194190302}")
    private String defaultFrom;

    private final RakenTracer rakenTracer;
    private final TwilioRestClient twilioRestClient;

    public TwilioClient(MeterRegistry registry,
                        RakenTracer rakenTracer,
                        TwilioRestClient twilioRestClient) {

        this.rakenTracer = rakenTracer;
        this.twilioRestClient = twilioRestClient;

        createMetrics(registry);
    }

    private void createMetrics(MeterRegistry registry) {
        twilioSuccess = registry.counter(CustomMetrics.TWILIO_SMS_SUCCESS);
        twilioError = registry.counter(CustomMetrics.TWILIO_SMS_ERROR);
    }

    public void sendSms(final SmsDeliveryRequest smsDeliveryRequest) {

        final String to = cleanNumber(smsDeliveryRequest.to);
        log.debug("Send message to " + to);

        try {

            final Message message = Message.creator(
                    new com.twilio.type.PhoneNumber(to),
                    new com.twilio.type.PhoneNumber(defaultFrom),
                    smsDeliveryRequest.body)
                    .create(twilioRestClient);

            log.debug("Sent sms message [{}] at {}", smsDeliveryRequest, message.getDateSent());
            twilioSuccess.increment();

        } catch (ApiException e) {

            rakenTracer.addTag(TraceTags.TWILIO_ERROR_CODE, e.getCode());
            rakenTracer.addTag(TraceTags.TWILIO_ERROR_CODE_INFO, "https://www.twilio.com/docs/api/errors/" + e.getCode());
            twilioError.increment();

            if( INVALID_TO_NUMBER_ERROR_CODES.contains(e.getCode()) ||
                    TwilioErrorCodes.CUSTOMER_UNSUBSCRIBED.equals(e.getCode()) ) {

                log.warn(appendEntries(
                        ImmutableMap.of(
                            "to", smsDeliveryRequest.to,
                            "twilio.error.code", e.getCode()
                        )),
                        "Undeliverable SMS"
                );
                throw new UndeliverableSmsException("to", e.getMessage());
            }

            log.error(
                appendEntries(
                    ImmutableMap.of(
                            "to", smsDeliveryRequest.to,
                            "twilio.error.code", e.getCode()
                )),
                "Error sending SMS message", e
            );

            throw new RuntimeException("Error sending SMS message", e);
        }
    }

    public Optional<PhoneNumber> validateNumber(final String phoneNumber) {

        try {

            return Optional.of(PhoneNumber
                    .fetcher(new com.twilio.type.PhoneNumber(phoneNumber))
                    .setType("carrier")
                    .fetch(twilioRestClient));

        } catch (ApiException e) {
            if ( e.getStatusCode() != null && e.getStatusCode() == 404 ) {
                return Optional.empty();
            } else {
                throw e;
            }
        }
    }

    static String cleanNumber(final String number) {
        return number.replaceAll("[^0-9+]", "");
    }
}
