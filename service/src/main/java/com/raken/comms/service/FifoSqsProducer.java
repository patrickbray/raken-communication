package com.raken.comms.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

public class FifoSqsProducer implements MessageProducer {

    private static final String ZIPKIN_TRACE_ID_NAME = "Zipkin-TraceId";

    static final String TRACE_ID_NAME = "X-B3-TraceId";
    static final String SPAN_ID_NAME = "X-B3-SpanId";

    @Resource
    private RakenTracer rakenTracer;

    @Resource
    private AmazonSQS amazonSqs;

    private String queueUrl;

    private String queueName;

    private String messageGroupId;

    @PostConstruct
    public void postConstruct() {

        this.queueUrl = amazonSqs.listQueues(queueName)
            .getQueueUrls().get(0);
    }

    public void queueMessage(final String json) {

        final SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(json);

        sendMessageRequest.addMessageAttributesEntry(ZIPKIN_TRACE_ID_NAME,
                new MessageAttributeValue()
                        .withDataType("String")
                        .withStringValue(String.valueOf(rakenTracer.getTraceIdString()))
        );

        sendMessageRequest.addMessageAttributesEntry(TRACE_ID_NAME,
                new MessageAttributeValue()
                        .withDataType("String")
                        .withStringValue(String.valueOf(rakenTracer.getTraceId()))
        );

        sendMessageRequest.addMessageAttributesEntry(SPAN_ID_NAME,
                new MessageAttributeValue()
                        .withDataType("String")
                        .withStringValue(String.valueOf(rakenTracer.getSpanId()))
        );

        sendMessageRequest.setMessageGroupId(messageGroupId);

        final SendMessageResult sendMessageResult = amazonSqs.sendMessage(sendMessageRequest);

        rakenTracer.addTag("MessageId", sendMessageResult.getMessageId());
    }

    @Override
    public String getQueueUrl() {
        return queueUrl;
    }

    @Override
    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public void setMessageGroupId(String messageGroupId) {
        this.messageGroupId = messageGroupId;
    }
}