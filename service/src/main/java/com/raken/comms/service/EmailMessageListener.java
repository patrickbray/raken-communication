package com.raken.comms.service;

import brave.ScopedSpan;
import brave.Span;
import brave.Tracer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.raken.comms.model.EmailDeliveryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jms.JMSException;
import java.io.IOException;

import static com.raken.comms.service.FifoSqsProducer.SPAN_ID_NAME;
import static com.raken.comms.service.FifoSqsProducer.TRACE_ID_NAME;

@Component
public class EmailMessageListener {

    private static final Logger log = LoggerFactory.getLogger(EmailMessageListener.class);
    
    @Resource
    private RakenTracer rakenTracer;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private EmailService emailService;

    @Resource
    private Tracer tracer;

    @JmsListener(destination = "${email.queue.name}", containerFactory = "emailJmsListenerContainerFactory")
    public void receiveEmailRequest(
            final String requestJSON,
            @Header(value = TRACE_ID_NAME, required = false) Long traceId,
            @Header(value = SPAN_ID_NAME, required = false) Long spanId) throws JMSException {

        final Span parentSpan = rakenTracer.createSpanFromIds(traceId, spanId);
        final ScopedSpan receiveEmailRequest = tracer.startScopedSpanWithParent("receive-email-request", parentSpan.context());

        try {

            log.debug("Received message={}", requestJSON);

            final EmailDeliveryRequest emailDeliveryRequest =
                    objectMapper.readValue(requestJSON, EmailDeliveryRequest.class);

            emailService.sendEmail(emailDeliveryRequest);

        } catch (final IOException ex) {

            log.error("Encountered error while parsing message.", ex);
            throw new JMSException("Encountered error while parsing message.");

        } finally { // note the scope is independent of the span
            receiveEmailRequest.finish();
        }
    }
}
