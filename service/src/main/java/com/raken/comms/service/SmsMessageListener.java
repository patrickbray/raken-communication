package com.raken.comms.service;

import brave.ScopedSpan;
import brave.Span;
import brave.Tracer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.raken.comms.model.SmsDeliveryRequest;
import com.raken.comms.exception.UndeliverableSmsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jms.JMSException;
import java.io.IOException;

import static com.raken.comms.service.FifoSqsProducer.SPAN_ID_NAME;
import static com.raken.comms.service.FifoSqsProducer.TRACE_ID_NAME;

@Component
public class SmsMessageListener {

    private static final Logger log = LoggerFactory.getLogger(SmsMessageListener.class);
    
    @Resource
    private RakenTracer rakenTracer;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private SmsService smsService;

    @Resource
    private Tracer tracer;

    @JmsListener(destination = "${sms.queue.name}", containerFactory = "smsJmsListenerContainerFactory")
    public void receiveSmsRequest(
            final String requestJSON,
            @Header(value = TRACE_ID_NAME, required = false) Long traceId,
            @Header(value = SPAN_ID_NAME, required = false) Long spanId) throws JMSException {

        final Span parentSpan = rakenTracer.createSpanFromIds(traceId, spanId);
        final ScopedSpan receiveSmsRequest = tracer.startScopedSpanWithParent("receive-sms-request", parentSpan.context());

        try {

            log.debug("Received message={}", requestJSON);

            final SmsDeliveryRequest smsDeliveryRequest =
                    objectMapper.readValue(requestJSON, SmsDeliveryRequest.class);

            smsService.sendSms(smsDeliveryRequest);

        } catch (final IOException ex) {

            log.error("Encountered error while parsing message.", ex);
            throw new JMSException("Encountered error while parsing message.");

        } catch (UndeliverableSmsException ignore) {

        } finally {
            receiveSmsRequest.finish();
        }
    }
}
