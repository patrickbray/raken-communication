package com.raken.comms.service;

import com.google.common.collect.ImmutableMap;
import com.raken.comms.model.HelpChatEvent;
import io.intercom.api.Company;
import io.intercom.api.Contact;
import io.intercom.api.ContactCollection;
import io.intercom.api.CustomAttribute;
import io.intercom.api.Event;
import io.intercom.api.IntercomException;
import io.intercom.api.JobItem;
import io.intercom.api.NotFoundException;
import io.intercom.api.User;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.raken.comms.util.IntercomJsonUtil.fromJson;
import static com.raken.comms.util.IntercomJsonUtil.jsonPatch;
import static com.raken.comms.util.IntercomJsonUtil.toJson;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
public class HelpChatService {

    private final static Logger logger = LoggerFactory.getLogger(HelpChatService.class);

    @Resource
    MessageProducer helpChatQueueProducer;

    public void queueEvent(HelpChatEvent crmEvent) {
        helpChatQueueProducer.queueMessage(
                toJson(crmEvent)
        );
    }

    public void syncUser(String email, User intercomUser, List<String> ignoreConflictAttrs, boolean createOnNotFound) {
        try {
            if (createOnNotFound || findUserByEmail(email).isPresent()) {
                updateIntercomWithIgnoreConflict(intercomUser, ignoreConflictAttrs);
            } else {
                throw new RuntimeException("sync user with createOnNotFound: false but can not find user by email: "+ email);
            }
        } catch (IntercomException e) {
            if (e.getMessage().contains("Multiple existing users match this email address")) {
                //pass through as we can't do much here
                logger.warn("find duplicate users in intercom by email: [{}]", email);
                return;
            }
            throw new RuntimeException(e);
        }
    }

    private void updateIntercomWithIgnoreConflict(User intercomUser, List<String> ignoreConflictAttrs) {
        Map<String, CustomAttribute> preservedKV = reserveHonorAttributes(intercomUser, ignoreConflictAttrs);

        removeCustomAttributes(intercomUser, preservedKV.keySet());
        User updatedUser = User.update(intercomUser);

        updatePreserveAttrIfNotPresented(ignoreConflictAttrs, preservedKV, updatedUser);
    }

    private void updatePreserveAttrIfNotPresented(List<String> ignoreConflictAttrs, Map<String, CustomAttribute> preservedKV, User updatedUser) {
        if (updatedUser != null) {
            long updated = ignoreConflictAttrs.stream().filter(attr -> {

                if (updatedUser.getCustomAttributes().get(attr) == null
                        && preservedKV.get(attr) != null) {

                    updatedUser.getCustomAttributes().put(
                            attr,
                            preservedKV.get(attr)
                    );
                    return true;
                }
                return false;
            }).count();

            if (updated > 0) {
                User.update(updatedUser);
            }

        }
    }

    private void removeCustomAttributes(User intercomUser, Set<String> keys) {
        keys.forEach( key -> intercomUser.getCustomAttributes().remove(key));
    }

    @NotNull
    private Map<String, CustomAttribute> reserveHonorAttributes(User intercomUser, List<String> honorRemoteAttributes) {
        return honorRemoteAttributes.stream()
                    .collect(Collectors.toMap(
                            attr -> attr,
                            attr -> intercomUser.getCustomAttributes().get(attr)
                    ));
    }

    public void syncCompany(Company company, boolean createOnNotFound) {
        if (createOnNotFound ||
                (isNotBlank(company.getCompanyID()) && findCompanyByUUID(company.getCompanyID()).isPresent())) {
            Company.update(company);
        } else {
            if (isNotBlank(company.getCompanyID())) {
                throw new RuntimeException("sync company with createOnNotFound: false but can not find company by uuid: " + company.getCompanyID());
            }
        }
    }

    private Optional<Company> findCompanyByUUID(String UUID) {
        return Optional.ofNullable(Company.find(
                            ImmutableMap.of(
                                    "company_id",
                                    UUID
                            )));
    }

    public void convert(String leadIdentifier, String userEmail){
        if (leadIdentifier == null) {
            convertByEmail(userEmail);
        } else {
            Contact.convert(
                    Contact.findByUserID(leadIdentifier),
                    User(userEmail)
            );
        }
    }

    private void convertByEmail(String email){
        ContactCollection contactCollection = Contact.listByEmail(email);
        if (contactCollection != null && contactCollection.hasNext()) {
            while (contactCollection.hasNext()) {
                // Update Contact if exists
                Contact contact = contactCollection.next();
                User userAfterConvert = Contact.convert(contact, User(email));
                if (userAfterConvert == null) {
                    logger.warn("Intercom user is null after lead {} conversion.", email);
                } else {
                    logger.debug("Intercom lead {} was successfully converted to user.", email);
                }
            }
        }
    }

    private User User(String email){
        User user = new User();
        user.setEmail(email);
        return user;
    }

    private Optional<User> findUserByEmail(String userEmail) {
        try{
            return Optional.of(User.find(ImmutableMap.of("email", userEmail)));
        }
        catch (NotFoundException e){
            return Optional.empty();
        }
    }

    public void syncLead(String contactJson, boolean createOnNotFound) {
        Contact contact = fromJson(contactJson, Contact.class);
        if (createOnNotFound ||
                (isNotBlank(contact.getUserID()) && Contact.findByUserID(contact.getUserID())!= null)) {
            Contact.update(contact);
        } else {
            if (isNotBlank(contact.getUserID())) {
                throw new RuntimeException("sync lead with createOnNotFound: false but can not find lead by userId: " + contact.getUserID());
            }
        }
    }

    public void syncLead(String email, String contactUpdate, boolean onEmptyCreate) {
        try {
            ContactCollection contactCollection = Contact.listByEmail(email);
            if (contactCollection != null && contactCollection.hasNext()) {
                while (contactCollection.hasNext()) {
                    // Update Contact if exists
                    Contact contact = contactCollection.next();
                    Contact updated = jsonPatch(contact, contactUpdate);
                    Contact updatedLead = Contact.update(updated);
                    if (updatedLead == null) {
                        logger.warn("Cannot update Intercom lead {}", email);
                    }
                }
            } else if (onEmptyCreate){
                Contact contact = new Contact();
                contact.setEmail(email);
                Contact updated = jsonPatch(contact, contactUpdate);
                Contact createdLead = Contact.create(updated);
                if (createdLead == null) {
                    logger.warn("Cannot create Intercom lead {}", email);
                }
            }
        } catch (Exception e) {
            logger.error("Cannot update intercom lead global unsubscribe attribute", e);
        }
    }

    public void syncEvent(Event event) {
        Event.create(event);
    }

    public void bulkDeleteUser(List<String> uuid) {
        Map<Integer, List<String>> requestMap = groupingList(uuid, 100);

        requestMap.forEach( (key, users) -> {
            List<JobItem<User>> jobs = users.stream()
                    .map(it -> new JobItem<>("delete", new User().setUserId(it)))
                    .collect(Collectors.toList());

            io.intercom.api.User.submit(jobs);
        } );
    }

    private Map<Integer, List<String>> groupingList(List<String> intercomUserIds, int subsize) {
        int groupCount = (intercomUserIds.size() / subsize) + 1;

        AtomicReference<Integer> index = new AtomicReference<>(0);
        return intercomUserIds.stream().collect(Collectors.groupingBy(
                user -> index.getAndSet(index.get() + 1) % groupCount,
                Collectors.toList()
        ));
    }

}
