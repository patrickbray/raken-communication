package com.raken.comms.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.raken.comms.util.HashUtils;
import com.raken.comms.util.UrlUtils;
import com.sendgrid.APICallback;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import lombok.Builder;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.crypto.Mac;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkArgument;
import static com.raken.comms.util.HashUtils.calculateHmacHash;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Service
public class SendGridContactManagementService {

    private static final Logger log = LoggerFactory.getLogger(SendGridContactManagementService.class);

    @Value("${email.whitelist:.+@rakenapp.com|.+@neklo.com}")
    private String emailWhiteList;

    @Value("${sendgrid.sandbox.mode:false}")
    private boolean sandboxMode;

    private Pattern emailWhitelistPattern;

    @Value("${rakenapp.base.url}")
    private String rakenappBaseUrl;

    @Value("${sendgrid.unsubscribe.secret}")
    private String sendgridUnsubscribeSecret;

    private Mac sha1Hmac;

    @Resource
    private SendGrid sendGrid;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private RakenTracer rakenTracer;

    @PostConstruct
    public void postConstruct() {
        sha1Hmac = HashUtils.getHmac(sendgridUnsubscribeSecret);
        this.emailWhitelistPattern = Pattern.compile(emailWhiteList);
    }

    @NewSpan("sendgrid-contact-update")
    void updateContactsUnsubscribeLinks(final Mail mail) {

        final List<Contact> contacts = mail.getPersonalization()
                .stream()
                .map(this::getEmailsFromPersonalisation)
                .flatMap(Collection::stream)
                .map(it -> Contact.builder()
                        .email(it)
                        .unsubscribeLink(makeUnsubscribeLink(it))
                        .build()
                ).collect(toList());

        if( isNotEmpty(contacts) ) {
            updateContacts(contacts);
        }
    }

    private List<String> getEmailsFromPersonalisation(final Personalization personalization) {
        return Stream.of(personalization.getTos(), personalization.getCcs(), personalization.getBccs())
                .flatMap(Collection::stream)
                .map(Email::getEmail)
                .filter(it -> !sandboxMode || emailWhitelistPattern.matcher(it).matches() )
                .collect(toList());
    }

    private void updateContacts(final List<Contact> contacts) {

        final Request request = new Request();
        request.setMethod(Method.PATCH);
        request.setEndpoint("contactdb/recipients");

        try {

            request.setBody(objectMapper.writeValueAsString(contacts));

            sendGrid.attempt(request, new APICallback() {
                @Override
                public void error(Exception ex) {
                    log.error("Exception while update global suppression in Sendgrid. contacts={}", contacts, ex);
                }

                @Override
                public void response(Response response) {
                    log.debug("SendGrid Response: statusCode={} body={} headers={}",
                            response.getStatusCode(),
                            response.getBody(),
                            response.getHeaders());

                    rakenTracer.addTag("sendgrid-status-code", response.getStatusCode());
                }
            });

        } catch (Exception e) {
            log.error("Exception while update global suppression in Sendgrid. contacts={}", contacts, e);
        }
    }

    private String makeUnsubscribeLink(@NotNull final String email) {

        checkArgument( isNotBlank(email) );

        return UrlUtils.makeUnsubscribeLink(rakenappBaseUrl, calculateHmacHash(sha1Hmac, email), email);
    }
}

@Builder
@ToString
class Contact {

    @JsonProperty("email")
    private String email;

    @JsonProperty("unsubscribe_link")
    private String unsubscribeLink;

}