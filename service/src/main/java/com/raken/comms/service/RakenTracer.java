package com.raken.comms.service;

import brave.Span;
import brave.Tracer;
import brave.propagation.TraceContext;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Optional;

@Component
public class RakenTracer {

    @Resource
    private Tracer tracer;

    public Span createSpanFromIds(Long traceId, Long spanId) {

        if( traceId == null || spanId == null ) {
            return tracer.nextSpan();
        }

        return tracer.joinSpan(
                TraceContext.newBuilder()
                        .traceId(traceId)
                        .spanId(spanId)
                        .parentId(spanId)
                        .sampled(true)
                        .build()
        );
    }

    public void addTag(final String key, final String value) {

        final Span span = tracer.currentSpan();

        if( span != null ) {
            span.tag(key, value);
        }
    }

    public void addTag(final String key, final Integer value) {
        addTag(key, String.valueOf(value));
    }

    @Nullable
    public Long getTraceId() {

        if (tracer.currentSpan() != null &&
                tracer.currentSpan().context() != null) {
            return tracer.currentSpan().context().traceId();
        }

        return null;
    }

    @Nullable
    public String getTraceIdString() {

        if (tracer.currentSpan() != null &&
                tracer.currentSpan().context() != null) {
            return tracer.currentSpan().context().traceIdString();
        }

        return null;
    }

    @Nullable
    public Long getSpanId() {

        if (tracer.currentSpan() != null &&
                tracer.currentSpan().context() != null) {
            return tracer.currentSpan().context().spanId();
        }

        return null;
    }

    public Optional<Span> getCurrentSpan() {
        return Optional.ofNullable(tracer.currentSpan());
    }
}
