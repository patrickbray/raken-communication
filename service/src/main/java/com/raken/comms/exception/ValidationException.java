package com.raken.comms.exception;

import com.raken.comms.model.ValidationDetail;

import java.util.Collections;
import java.util.List;

public class ValidationException extends RuntimeException {
    private List<ValidationDetail> validationDetails;

    public ValidationException(String field, String fieldMessage) {
        this(Collections.singletonList(new ValidationDetail(field, fieldMessage)));
    }

    public ValidationException(ValidationDetail validationDetail) {
        this(Collections.singletonList(validationDetail));
    }

    public ValidationException(List<ValidationDetail> validationDetails) {
        super("Validation errors");
        this.validationDetails = validationDetails;
    }

    public List<ValidationDetail> getValidationDetails() {
        return validationDetails;
    }
}