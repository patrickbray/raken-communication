package com.raken.comms.exception;

public class UndeliverableSmsException extends ValidationException {

    public UndeliverableSmsException(String field, String fieldMessage) {
        super(field, fieldMessage);
    }
}
