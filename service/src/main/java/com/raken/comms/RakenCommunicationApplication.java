package com.raken.comms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@SpringBootApplication
public class RakenCommunicationApplication {

    private static final Logger log = LoggerFactory.getLogger(RakenCommunicationApplication.class);

    private static final String SPRING_PROFILES_ACTIVE = "spring.profiles.active";
    private static final String ENV = "env";

    public static void main(String[] args) {

        final String activeProfiles = System.getProperty(SPRING_PROFILES_ACTIVE);

        if ( isEmpty(activeProfiles) ) {
            log.warn("System property 'spring.profiles.active' not set from JVM arg, initially force setting it to local");
            System.setProperty(SPRING_PROFILES_ACTIVE, "local");
            System.setProperty(ENV, "local");
        }

        if ( isEmpty(System.getProperty("aws.region")) ) {
            log.warn("System property 'aws.region' not set from JVM arg, initially force setting it to default us-west-2");
            System.setProperty("aws.region", "us-west-2");
        }

        SpringApplication.run(RakenCommunicationApplication.class, args);
    }
}
