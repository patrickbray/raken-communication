package com.raken.comms.config;

import com.raken.comms.service.FifoSqsProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailConfig {

    @Value("${email.queue.name}")
    private String emailQueueName;

    @Value("${email.sqs.message.group.id:EMAIL}")
    private String emailSqsMessageGroupId;

    @Bean
    public FifoSqsProducer emailQueueProducer() {
        final FifoSqsProducer fifoSqsProducer = new FifoSqsProducer();
        fifoSqsProducer.setMessageGroupId(emailSqsMessageGroupId);
        fifoSqsProducer.setQueueName(emailQueueName);
        return fifoSqsProducer;
    }
}
