package com.raken.comms.config;

import com.raken.comms.service.RakenTracer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.sleuth.SpanAdjuster;
import org.springframework.cloud.sleuth.instrument.web.TraceWebServletAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;

@Configuration
public class TracingConfig {

    @Value("${env}")
    private String env;

    @Bean
    SpanAdjuster environmentTagger() {
        return span -> span.toBuilder()
                .putTag("env", env)
                .build();
    }
}

/**
 * Filter to add the trace id to the HTTP Response
 */
@Component
@Order(TraceWebServletAutoConfiguration.TRACING_FILTER_ORDER + 1)
class RakenTraceFilter extends GenericFilterBean {

    @Resource
    private RakenTracer rakenTracer;

    @Resource
    private RakenTracingConfig rakenTracingConfig;

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain) throws IOException, ServletException {

        ((HttpServletResponse) response).addHeader("X-B3-TraceId", rakenTracer.getTraceIdString());
        chain.doFilter(request, response);

        rakenTracer.getCurrentSpan().ifPresent(span ->
                rakenTracingConfig.getHeaders()
                        .forEach(header -> {
                            Optional.ofNullable(((HttpServletRequest) request).getHeader(header))
                                .ifPresent(value -> span.tag(header, value));
                            ;
                        })
        );
    }
}

@Configuration
@ConfigurationProperties(prefix = "raken.tracing")
class RakenTracingConfig {

    /**
     * Additional headers that should be added as tags if they exist. If the header
     * value is multi-valued, the tag value will be a comma-separated, single-quoted
     * list.
     */
    private Collection<String> headers = new LinkedHashSet<>();

    public Collection<String> getHeaders() {
        return this.headers;
    }

    public void setHeaders(Collection<String> headers) {
        this.headers = headers;
    }

}