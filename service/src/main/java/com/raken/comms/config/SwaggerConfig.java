package com.raken.comms.config;

import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.raken.comms.model.ErrorResponse;
import com.raken.comms.model.ValidationDetail;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.configuration.ObjectMapperConfigured;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.URI;
import java.time.Instant;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

@Configuration
@EnableSwagger2
@Import({BeanValidatorPluginsConfiguration.class})
public class SwaggerConfig {

    @Bean
    ApplicationListener<ObjectMapperConfigured> objectMapperConfiguredApplicationListener() {
        return (ObjectMapperConfigured event) -> {
            ObjectMapper objectMapper = event.getObjectMapper();
            customizeObjectMapper(objectMapper);
        };
    }

    private void customizeObjectMapper(ObjectMapper objectMapper) {
        SimpleModule module = new SimpleModule();
        objectMapper.registerModule(module);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    @Bean
    public Docket rakenApi() {

        final TypeResolver typeResolver = new TypeResolver();

        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Raken-Communications-Api")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.raken.comms.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .pathMapping("/")
                .ignoredParameterTypes(ApiIgnore.class)
                .enableUrlTemplating(false)
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.POST, asList(
                        new ResponseMessageBuilder()
                            .code(400)
                            .message("Bad Request")
                            .responseModel(new ModelRef("ErrorResponse"))
                            .build(),
                        new ResponseMessageBuilder()
                            .code(500)
                            .message("Internal Server Error")
                            .responseModel(new ModelRef("ErrorResponse"))
                            .build()
                    )
                )
                .globalResponseMessage(RequestMethod.DELETE, singletonList(new ResponseMessageBuilder()
                        .code(204)
                        .message("Object was deleted")
                        .build()))
                .directModelSubstitute(Instant.class, Integer.class)
                .directModelSubstitute(URI.class, String.class)
                .additionalModels(
                        typeResolver.resolve(ErrorResponse.class),
                        typeResolver.resolve(ValidationDetail.class)
                );
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Raken Communications API")
                .description("Raken Communications API Endpoint Documentation")
                .termsOfServiceUrl("https://www.rakenapp.com/")
                .version("1.0.0")
                .build();
    }
}
