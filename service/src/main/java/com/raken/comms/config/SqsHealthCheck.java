package com.raken.comms.config;

import com.amazonaws.services.sqs.AmazonSQS;
import com.raken.comms.service.MessageProducer;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SqsHealthCheck implements HealthIndicator {

    @Resource
    private List<MessageProducer> sqsMessageProducers;

    @Resource
    private AmazonSQS amazonSQS;

    @Override
    public Health health() {

        for (final MessageProducer it : sqsMessageProducers) {
            try {
                amazonSQS.getQueueUrl(it.getQueueName());
            } catch (RuntimeException e) {
                return Health.down(e)
                        .withDetail("Unable to connect to queue ", it.getQueueUrl())
                        .build();
            }
        }

        return Health.up()
                .withDetail("Connectivity to ",
                        sqsMessageProducers.stream()
                            .map(MessageProducer::getQueueUrl)
                                .collect(Collectors.toList()))
                .build();
    }
}