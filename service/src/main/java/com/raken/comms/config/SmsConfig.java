package com.raken.comms.config;

import com.raken.comms.service.FifoSqsProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmsConfig {

    @Value("${sms.queue.name}")
    private String smsQueueName;

    @Value("${sms.sqs.message.group.id:SMS}")
    private String smsSqsMessageGroupId;

    @Bean
    public FifoSqsProducer smsQueueProducer() {
        final FifoSqsProducer fifoSqsProducer = new FifoSqsProducer();
        fifoSqsProducer.setMessageGroupId(smsSqsMessageGroupId);
        fifoSqsProducer.setQueueName(smsQueueName);
        return fifoSqsProducer;
    }
}
