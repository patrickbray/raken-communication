package com.raken.comms.config;

import com.raken.comms.service.FifoSqsProducer;
import io.intercom.api.Intercom;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class ChatConfig {

    @Value("${helpchat.queue.name}")
    private String helpChatQueueName;

    @Value("${helpchat.sqs.message.group.id:HELPCHAT}")
    private String helpChatSqsMessageGroupId;

    @Value("${intercom.access.token}")
    private String personalAccessToken;

    @Value("${default.connection.timeout}")
    private int connectionTimeout;

    @Value("${default.read.timeout}")
    private int readTimeout;

    @PostConstruct
    public void init() {
        Intercom.setToken(personalAccessToken);
        Intercom.setConnectionTimeout(connectionTimeout);
        Intercom.setRequestTimeout(readTimeout);
    }

    @Bean
    public FifoSqsProducer helpChatQueueProducer() {
        final FifoSqsProducer fifoSqsProducer = new FifoSqsProducer();
        fifoSqsProducer.setMessageGroupId(helpChatSqsMessageGroupId);
        fifoSqsProducer.setQueueName(helpChatQueueName);
        return fifoSqsProducer;
    }
}

