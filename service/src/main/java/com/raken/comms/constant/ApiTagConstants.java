package com.raken.comms.constant;

public interface ApiTagConstants {

    String EMAIL = "email";
    String SMS = "sms";
    String HELPCHAT = "helpchat";
}
