package com.raken.comms.constant;

public interface TraceTags {

    String SENDGRID_MESSAGE_ID = "sendgrid-message-id";
    String SENDGRID_STATUS_CODE = "sendgrid-status-code";
    String SENDGRID_SAND_BOXED = "sendgrid-sand-boxed";

    String TWILIO_ERROR_CODE = "twilio.error.code";
    String TWILIO_ERROR_CODE_INFO = "twilio.error.code.info";
}
