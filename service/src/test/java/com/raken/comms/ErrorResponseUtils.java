package com.raken.comms;

import com.raken.comms.model.ErrorResponse;
import com.raken.comms.model.ValidationDetail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

class ErrorResponseUtils {

    static ValidationDetail getSingleValidationDetail(final ErrorResponse body) {

        assertNotNull(body);
        assertNotNull(body.getValidationDetails());
        assertEquals(1, body.getValidationDetails().size());

        return body.getValidationDetails().get(0);
    }
}
