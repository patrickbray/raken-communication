package com.raken.comms.service;

import com.github.javafaker.Faker;
import com.raken.comms.model.SmsDeliveryRequest;
import com.raken.comms.exception.UndeliverableSmsException;
import com.raken.comms.service.TwilioClient.CustomMetrics;
import com.raken.comms.service.TwilioClient.TwilioErrorCodes;
import com.twilio.exception.ApiException;
import com.twilio.http.TwilioRestClient;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TwilioClientTest {

    private final Faker faker = new Faker();

    private TwilioClient twilioClient;

    @Mock
    private MeterRegistry meterRegistry;

    @Mock
    private TwilioRestClient twilioRestClient;

    @Mock
    private RakenTracer rakenTracer;

    @Mock
    private Counter twilioSmsError;

    @Before
    public void setUp() {

        when(meterRegistry.counter(CustomMetrics.TWILIO_SMS_ERROR)).thenReturn(twilioSmsError);
        when(meterRegistry.counter(CustomMetrics.TWILIO_SMS_SUCCESS)).thenReturn(mock(Counter.class));

        this.twilioClient = new TwilioClient(meterRegistry, rakenTracer, twilioRestClient);
    }

    @Test
    public void testCustomerUnsubscribed() {

        final SmsDeliveryRequest sms = SmsDeliveryRequest.builder()
                .to(getValidE164PhoneNumber())
                .body(faker.yoda().quote())
                .build();

        when(twilioRestClient.request(any())).thenThrow(
                new ApiException("", TwilioErrorCodes.CUSTOMER_UNSUBSCRIBED, "", 400, null)
        );

        try {
            twilioClient.sendSms(sms);
            fail("Expected UndeliverableSMSException");
        } catch ( UndeliverableSmsException ignore) {}

        verify(twilioSmsError).increment();
    }

    @NotNull
    private String getValidE164PhoneNumber() {
        return "+1"+faker.number().randomNumber(14, true);
    }
}