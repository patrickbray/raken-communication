package com.raken.comms.service;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@RunWith(MockitoJUnitRunner.class)
public class SendGridClientTest {

    @Spy
    @InjectMocks
    private SendGridClient sendGridClient;

    @Mock
    private SendGrid sendGrid;

    @Mock
    private RakenTracer rakenTracer;

    @Captor
    private ArgumentCaptor<Mail> mail;

    @Before
    public void setUp() {

        setField(sendGridClient, "sandboxMode", true);
        setField(sendGridClient, "emailWhiteList", ".+@rakenapp.com|.+@neklo.com");

        sendGridClient.postConstruct();
    }

    @Test
    public void testSendGridSandboxMode() throws IOException {

        when(sendGrid.api(any(Request.class)))
                .thenReturn(SendGridTestDataFactory.emailSentResponse());

        sendGridClient.sendEmail(
                SendGridTestDataFactory.mailTo("john.smith@gmail.com")
        );

        verify(sendGridClient).sendMail(mail.capture());

        MailAssertions.assertSandboxModeIsEnabled(mail.getValue());
    }

    @Test
    public void testEmailWhiteList_Raken() throws IOException  {

        when(sendGrid.api(any(Request.class)))
                .thenReturn(SendGridTestDataFactory.emailSentResponse());

        sendGridClient.sendEmail(
                SendGridTestDataFactory.mailTo("john.smith@rakenapp.com")
        );

        verify(sendGridClient).sendMail(mail.capture());

        MailAssertions.assertSandBoxModeIsDisabled(mail.getValue());
    }

    @Test
    public void testEmailWhiteList_Neklo() throws IOException  {

        when(sendGrid.api(any(Request.class)))
                .thenReturn(SendGridTestDataFactory.emailSentResponse());

        sendGridClient.sendEmail(
                SendGridTestDataFactory.mailTo("john.smith@neklo.com")
        );

        verify(sendGridClient).sendMail(mail.capture());

        MailAssertions.assertSandBoxModeIsDisabled(mail.getValue());
    }

    @Test
    public void testEmailWhiteList_Cc() throws IOException  {

        when(sendGrid.api(any(Request.class)))
                .thenReturn(SendGridTestDataFactory.emailSentResponse());

        final Mail mail = SendGridTestDataFactory.mailTo("john.smith@rakenapp.com");
        mail.getPersonalization().get(0).addCc(new Email("john.smith@gmail.com"));

        sendGridClient.sendEmail(mail);

        verify(sendGridClient).sendMail(this.mail.capture());

        MailAssertions.assertSandboxModeIsEnabled(this.mail.getValue());
    }

    @Test
    public void testEmailWhiteList_Bcc() throws IOException  {

        when(sendGrid.api(any(Request.class)))
                .thenReturn(SendGridTestDataFactory.emailSentResponse());

        final Mail mail = SendGridTestDataFactory.mailTo("john.smith@rakenapp.com");
        mail.getPersonalization().get(0).addBcc(new Email("john.smith@gmail.com"));

        sendGridClient.sendEmail(mail);

        verify(sendGridClient).sendMail(this.mail.capture());

        MailAssertions.assertSandboxModeIsEnabled(this.mail.getValue());
    }

    static class MailAssertions {

        static void assertSandBoxModeIsDisabled(final Mail mail) {
            if(mail.getMailSettings() != null &&
                    mail.getMailSettings().getSandBoxMode() != null) {
                assertFalse(mail.getMailSettings().getSandBoxMode().getEnable());
            }
        }

        static void assertSandboxModeIsEnabled(Mail mail) {
            assertTrue(mail.getMailSettings().getSandBoxMode().getEnable());
        }
    }

    static class SendGridTestDataFactory {

        @NotNull
        static Response emailSentResponse() {
            return new Response(200, "Email Sent", new HashMap<>());
        }

        @NotNull
        static Mail mailTo(final String emailTo) {
            return new Mail(new Email("no-reply@rakenapp.com"),
                    "Test Subject",
                    new Email(emailTo),
                    new Content(MediaType.TEXT_HTML.getType(), "Test email content"));
        }
    }
}