package com.raken.comms.service;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SmsServiceTest {

    @Test
    public void cleanNumber() {
        assertEquals("+64272345678", TwilioClient.cleanNumber("+64272345678"));
        assertEquals("+64272345678", TwilioClient.cleanNumber("+64 27 234 5678"));
    }
}