package com.raken.comms;

import io.github.swagger2markup.Swagger2MarkupConverter;
import io.github.swagger2markup.Swagger2MarkupExtensionRegistry;
import io.github.swagger2markup.builder.Swagger2MarkupExtensionRegistryBuilder;
import io.github.swagger2markup.spi.SwaggerModelExtension;
import io.swagger.models.Swagger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("local,test")
@RunWith(SpringRunner.class)
public class ApiDocGenerationTest {

    @LocalServerPort
    private int port;

    @Test
    public void generateApiDocs() throws MalformedURLException {

        final Path outputDirectory = Paths.get("build/asciidoc/generated");

        final Swagger2MarkupExtensionRegistry registry = new Swagger2MarkupExtensionRegistryBuilder()
                .withSwaggerModelExtension(new HostSwaggerModelExtension())
                .build();

        Swagger2MarkupConverter
                .from(new URL("http://localhost:"+port+"/v2/api-docs?group=Raken-Communications-Api"))
                .withExtensionRegistry(registry)
                .build()
                .toFolder(outputDirectory);
    }
}

class HostSwaggerModelExtension extends SwaggerModelExtension {

    public void apply(Swagger swagger) {
        swagger.setHost("comms.dev.rakenapp.com");
    }
}
