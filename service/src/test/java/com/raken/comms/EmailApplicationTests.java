package com.raken.comms;

import com.github.javafaker.Faker;
import com.raken.comms.model.EmailDeliveryRequest;
import com.raken.comms.model.ErrorResponse;
import com.raken.comms.model.ValidationDetail;
import com.raken.comms.service.SendGridClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static com.raken.comms.ErrorResponseUtils.getSingleValidationDetail;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("local,test")
@RunWith(SpringRunner.class)
public class EmailApplicationTests {

    private static final int FIVE_SECONDS = 5000;

    private static final String EMAIL = "john.smith@example.com";
    private static final String INVALID_EMAIL = "invalid-email";

    private Faker faker = new Faker();

    @Resource
    private TestRestTemplate restTemplate;

    @MockBean
    private SendGridClient sendGridClient;

    @Test
    public void testSendEmail_invalidTo() {

        final EmailDeliveryRequest request = EmailDeliveryRequest.builder()
                .to(INVALID_EMAIL)
                .subject(faker.yoda().quote())
                .body(faker.rickAndMorty().quote())
                .build();

        final ResponseEntity<ErrorResponse> responseEntity =
                restTemplate.postForEntity("/email?sync=true", request, ErrorResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

        final ValidationDetail validationDetail = getSingleValidationDetail(responseEntity.getBody());

        assertEquals("to", validationDetail.field);

        verifyZeroInteractions(sendGridClient);
    }

    @Test
    public void testSendEmail_Valid() {

        final EmailDeliveryRequest request = EmailDeliveryRequest.builder()
                .to(EMAIL)
                .subject(faker.yoda().quote())
                .body(faker.rickAndMorty().quote())
                .build();

        request.cc = null;

        final ResponseEntity<ErrorResponse> responseEntity =
                restTemplate.postForEntity("/email?sync=true", request, ErrorResponse.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    public void testSendEmail() {

        final EmailDeliveryRequest request = EmailDeliveryRequest.builder()
                .to(EMAIL)
                .cc(EMAIL)
                .bcc(EMAIL)
                .subject(faker.yoda().quote())
                .body(faker.rickAndMorty().quote())
                .build();

        final ResponseEntity<Void> responseEntity =
                restTemplate.postForEntity("/email?sync=true", request, Void.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        verify(sendGridClient).sendEmail(
                argThat(mail ->
                        mail.getSubject().equalsIgnoreCase(request.subject) &&
                                mail.personalization.size() == 1 &&
                                mail.personalization.get(0).getTos().get(0).getEmail().equalsIgnoreCase(EMAIL) &&
                                mail.content.get(0).getValue().equalsIgnoreCase(request.body)
                )
        );
    }

    @Test
    public void testSendEmail_Async() {

        final EmailDeliveryRequest request = EmailDeliveryRequest.builder()
                .to(EMAIL)
                .subject(faker.yoda().quote())
                .body(faker.rickAndMorty().quote())
                .build();

        final ResponseEntity<Void> responseEntity =
                restTemplate.postForEntity("/email", request, Void.class);

        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());

        verify(sendGridClient, timeout(FIVE_SECONDS)).sendEmail(
                argThat( mail ->
                        mail.getSubject().equalsIgnoreCase(request.subject) &&
                                mail.personalization.size() == 1 &&
                                mail.personalization.get(0).getTos().get(0).getEmail().equalsIgnoreCase(EMAIL) &&
                                mail.content.get(0).getValue().equalsIgnoreCase(request.body)
                )
        );
    }
}
