package com.raken.comms;

import com.github.javafaker.Faker;
import com.raken.comms.model.EmailDeliveryRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("local,test")
@RunWith(SpringRunner.class)
public class EmailIntegrationTest {

    private Faker faker = new Faker();

    @Resource
    private TestRestTemplate restTemplate;

    @Test
    public void testSendEmail() {

        final EmailDeliveryRequest emailDeliveryRequest = EmailDeliveryRequest.builder()
                .to("john.smith+1@example.com")
                .cc("john.smith+2@example.com")
                .bcc("john.smith+3@example.com")
                .subject(faker.ancient().hero())
                .body(faker.rickAndMorty().quote())
                .build();

        final ResponseEntity<Void> responseEntity =
                restTemplate.postForEntity("/email?sync=true", emailDeliveryRequest, Void.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}
