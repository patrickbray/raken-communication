package com.raken.comms;

import com.github.javafaker.Faker;
import com.raken.comms.model.ErrorResponse;
import com.raken.comms.model.SmsDeliveryRequest;
import com.raken.comms.model.ValidationDetail;
import com.raken.comms.service.TwilioClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static com.raken.comms.ErrorResponseUtils.getSingleValidationDetail;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("local,test")
@RunWith(SpringRunner.class)
public class SmsApplicationTests {

    private static final int FIVE_SECONDS = 5000;

    private final Faker faker = new Faker();

    @Resource
    private TestRestTemplate restTemplate;

    @MockBean
    private TwilioClient twilioClient;

    @Test
    public void testInvalidSms() {

        final SmsDeliveryRequest smsDeliveryRequest = SmsDeliveryRequest.builder()
                .to("+5")
                .body(faker.chuckNorris().fact())
                .build();

        final ResponseEntity<ErrorResponse> responseEntity =
                restTemplate.postForEntity("/sms?sync=true", smsDeliveryRequest, ErrorResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());

        final ValidationDetail validationDetail = getSingleValidationDetail(responseEntity.getBody());

        assertEquals("to", validationDetail.field);
        assertEquals("Not a valid E.164 phone number", validationDetail.fieldMessage);

        verifyZeroInteractions(twilioClient);
    }

    @Test
    public void testSendSms() {

        final SmsDeliveryRequest smsDeliveryRequest = SmsDeliveryRequest.builder()
                .to(getValidE164MobileNumber())
                .body(faker.chuckNorris().fact())
                .build();

        final ResponseEntity<Void> responseEntity =
                restTemplate.postForEntity("/sms?sync=true", smsDeliveryRequest, Void.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        verify(twilioClient).sendSms(smsDeliveryRequest);
    }

    private String getValidE164MobileNumber() {
        return "+16194190302";
    }

    @Test
    public void testSendSms_Async() {

        final SmsDeliveryRequest smsDeliveryRequest = SmsDeliveryRequest.builder()
                .to(getValidE164MobileNumber())
                .body(faker.chuckNorris().fact())
                .build();

        final ResponseEntity<Void> responseEntity =
                restTemplate.postForEntity("/sms", smsDeliveryRequest, Void.class);

        assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());

        verify(twilioClient, timeout(FIVE_SECONDS)).sendSms(smsDeliveryRequest);
    }
}
