package com.raken.comms.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.elasticmq.NodeAddress;
import org.elasticmq.rest.sqs.SQSRestServer;
import org.elasticmq.rest.sqs.SQSRestServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.concurrent.ThreadLocalRandom;

@Configuration
public class TestSqsConfig {

    private static final Logger log = LoggerFactory.getLogger(TestSqsConfig.class);

    @Value("${sms.queue.name}")
    private String smsQueueName;

    @Value("${email.queue.name}")
    private String emailQueueName;

    @Value("${helpchat.queue.name}")
    private String helpChatQueueName;

    private int port = ThreadLocalRandom.current().nextInt(5000, 10000);

    @Bean(destroyMethod = "stopAndWait")
    public SQSRestServer sqsRestServer() {

        final SQSRestServer sqsRestServer = SQSRestServerBuilder.withPort(port)
                .withServerAddress(new NodeAddress("http", "localhost", port, ""))
                .start();

        sqsRestServer.waitUntilStarted();

        log.info("SQS server started on port {}", port);

        return sqsRestServer;
    }

    @Bean
    @Primary
    @SuppressWarnings("unused")
    public AmazonSQS amazonSQS(SQSRestServer sqsRestServer) {

        final String endpoint = "http://localhost:" + port;

        final AmazonSQS amazonSQSClient = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("x", "x")))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, "us-west-2"))
                .build();

        amazonSQSClient.createQueue(emailQueueName);
        amazonSQSClient.createQueue(smsQueueName);
        amazonSQSClient.createQueue(helpChatQueueName);

        return amazonSQSClient;
    }
}
