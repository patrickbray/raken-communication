package com.raken.comms;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("local,test")
@RunWith(SpringRunner.class)
public class RakenCommunicationApplicationTests {

    @Resource
    private TestRestTemplate restTemplate;

    @Test
    public void contextLoads() {

        final ResponseEntity<String> healthCheckResponse =
                restTemplate.getForEntity("/admin/health", String.class);

        assertEquals(HttpStatus.OK, healthCheckResponse.getStatusCode());
    }
}
