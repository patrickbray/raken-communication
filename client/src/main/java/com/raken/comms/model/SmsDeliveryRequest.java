package com.raken.comms.model;

import com.raken.comms.validation.PhoneNumber;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SmsDeliveryRequest {

    @NotBlank
    @PhoneNumber
    @ApiModelProperty(position = 0,
            required = true,
            example = "+15005550006",
            notes = "Expects an E.164 formatted telephone number. https://www.twilio.com/docs/glossary/what-e164")
    public String to;

    @Size(max = 918)
    @ApiModelProperty(position = 1, required = true,
            example = "Your timecard for 2018-09-17 - 2018-09-21 is ready to sign at http://rakenapp.com/signoff/xscdbvrb")
    public String body;

    @Override
    public String toString() {
        return "SmsDeliveryRequest{" +
                "to='" + to + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}