package com.raken.comms.model;

import com.raken.comms.validation.EmailCollection;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Singular;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmailDeliveryRequest {

    @NotEmpty
    @Singular("to")
    @EmailCollection
    @ApiModelProperty(position = 0,
            required = true,
            example = "[ \"john.smith@example.com\"]")
    public List<String> to;

    @Singular("cc")
    @EmailCollection
    @ApiModelProperty(position = 1, example = "[ \"john.smith@example.com\"]")
    public List<String> cc;

    @Singular("bcc")
    @EmailCollection
    @ApiModelProperty(position = 2, example = "[ \"john.smith@example.com\"]")
    public List<String> bcc;

    @NotBlank
    @ApiModelProperty(position = 3, required = true, example = "Timecard for 2018-09-17 - 2018-09-21 is ready to sign")
    public String subject;

    @NotBlank
    @ApiModelProperty(position = 4, required = true)
    public String body;

    @ApiModelProperty(position = 5,
            example = "[ \"Daily Reports\"]")
    @Singular("category")
    public List<String> categories;

    @ApiModelProperty(position = 6,
            example = "1459")
    public Integer unsubscribeGroup;

    @Override
    public String toString() {
        return "EmailDeliveryRequest{" +
                "to=" + to +
                ", cc=" + cc +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
