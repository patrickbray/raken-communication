package com.raken.comms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ErrorResponse {

    @ApiModelProperty(position = 1, example = "327f83ac-9311-4517-a995-d56567960a4e")
    private String uniqueId;

    @ApiModelProperty(position = 2, example = "1495299250999")
    private long timestamp;

    @ApiModelProperty(position = 3, example = "400")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Integer code;

    @ApiModelProperty(position = 4, example = "Bad Request")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String message;

    @ApiModelProperty(position = 5, value = "List of fields which failed validation")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ValidationDetail> validationDetails;

    @SuppressWarnings("unused")
    public ErrorResponse() { }

    private ErrorResponse(String message) {
        this(message, null, null);
    }

    public ErrorResponse(String message, int code, String traceId) {
        this(message, code, traceId, null);
    }

    private ErrorResponse(String message, String traceId, List<ValidationDetail> validationDetails) {
        this(message, null, traceId, validationDetails);
    }

    public ErrorResponse(String message, Integer code, String traceId, List<ValidationDetail> validationDetails) {
        this.message = message;
        this.code = code;
        this.validationDetails = validationDetails;
        uniqueId = traceId == null? UUID.randomUUID().toString(): traceId;
        timestamp = new Date().getTime();
    }

    public ErrorResponse(Exception e) {
        this(e.getMessage());
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ValidationDetail> getValidationDetails() {
        return validationDetails;
    }

    public void setValidationDetails(List<ValidationDetail> validationDetails) {
        this.validationDetails = validationDetails;
    }
}
