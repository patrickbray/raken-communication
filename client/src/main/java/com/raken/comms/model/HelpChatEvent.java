package com.raken.comms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HelpChatEvent {

    private HelpChatEventType type;

    @NotBlank
    private String identifier;

    private String data;

    @Builder.Default
    private SyncOptions options = SyncOptions.builder().build();

}
