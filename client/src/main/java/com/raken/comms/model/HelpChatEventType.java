package com.raken.comms.model;

public enum HelpChatEventType {
    SYNC_USER, SYNC_COMPANY, SYNC_LEAD, SYNC_LEAD_BY_EMAIL, CONVERT_LEAD, SYNC_EVENT, BULK_DELETE_USERS
}
