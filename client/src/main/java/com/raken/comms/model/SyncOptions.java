package com.raken.comms.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SyncOptions {

    @Builder.Default
    private Boolean createOnNotFound = true;

    @Builder.Default
    private List<String> ignoreOnConflictAttrs = new ArrayList<>();

}
