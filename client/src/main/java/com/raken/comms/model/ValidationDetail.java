package com.raken.comms.model;

import io.swagger.annotations.ApiModelProperty;

public class ValidationDetail {

    @ApiModelProperty(position = 1, example = "name", value="The field an error is occurring on")
    public String field;

    @ApiModelProperty(position = 2, example = "Name is required", value="A message related to this error that is intended to be customer facing")
    public String fieldMessage;

    @SuppressWarnings("unused")
    public ValidationDetail() {}

    public ValidationDetail(final String field, final String fieldMessage) {
        this.field = field;
        this.fieldMessage = fieldMessage;
    }
}