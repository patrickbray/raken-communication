package com.raken.comms.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

public class EmailCollectionValidator implements ConstraintValidator<EmailCollection, Collection<String>> {

    private final EmailValidator validator = new EmailValidator();

    @Override
    public void initialize(EmailCollection constraintAnnotation) {
    }

    @Override
    public boolean isValid(Collection<String> value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        return value.stream().allMatch(it -> validator.isValid(it, context));
    }
}
