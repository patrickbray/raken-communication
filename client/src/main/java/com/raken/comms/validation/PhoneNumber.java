package com.raken.comms.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Pattern(regexp = "^\\+?[1-9]\\d{1,14}$")
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Constraint(validatedBy = { })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ReportAsSingleViolation
public @interface PhoneNumber {

    String message() default "Not a valid E.164 phone number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
