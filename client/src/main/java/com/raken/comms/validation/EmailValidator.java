package com.raken.comms.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Email;
import java.net.IDN;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator implements ConstraintValidator<Email, CharSequence> {
    private static String ATOM = "[a-z0-9!#$%&'*+/=?^_`{|}~-]";
    private static String DOMAIN;
    private static String IP_DOMAIN;
    private final Pattern localPattern;
    private final Pattern domainPattern;

    public EmailValidator() {
        this.localPattern = Pattern.compile(ATOM + "+(\\." + ATOM + "+)*", 2);
        this.domainPattern = Pattern.compile(DOMAIN + "|" + IP_DOMAIN, 2);
    }

    public void initialize(Email annotation) {
    }

    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (value != null && value.length() != 0) {
            String[] emailParts = value.toString().split("@", 3);
            if (emailParts.length != 2) {
                return false;
            } else if (!emailParts[0].endsWith(".") && !emailParts[1].endsWith(".")) {
                return !this.matchPart(emailParts[0], this.localPattern) ? false : this.matchPart(emailParts[1], this.domainPattern);
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private boolean matchPart(String part, Pattern pattern) {
        try {
            part = IDN.toASCII(part);
        } catch (IllegalArgumentException var4) {
            return false;
        }

        Matcher matcher = pattern.matcher(part);
        return matcher.matches();
    }

    static {
        DOMAIN = ATOM + "+(\\." + ATOM + "+)*";
        IP_DOMAIN = "\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\]";
    }
}
