#!/usr/bin/env bash
set -x

CONFIG_FILE=./query.text #location of the query definition
QUERY_ID=15892 #query id of sematext savedQuery to update
API_KEY="88c61d4c-8d65-4a15-bbd5-30977f9cf653"

ORIGINAL_QUERY_JSON=`curl -s -X GET "https://apps.sematext.com/users-web/api/v3/apps/16728/savedQueries" -H "accept: application/json" -H "Authorization: apiKey ${API_KEY}" | jq ".data.savedQueries[] | select(.id==\"${QUERY_ID}\")"`
FILTERED_COMMENTS=`sed -e 's/#.*$//' -e '/^$/d' ${CONFIG_FILE} | paste -sd " " -`

jq '.queryString = $updatedQuery' --arg updatedQuery "${FILTERED_COMMENTS}" <<< "$ORIGINAL_QUERY_JSON" > data.json

curl -X PUT \
  https://apps.sematext.com/users-web/api/v3/savedQueries/${QUERY_ID} \
  -H "Authorization: apiKey ${API_KEY}" \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d @data.json

rm data.json
