#!/usr/bin/env bash

API_KEY="88c61d4c-8d65-4a15-bbd5-30977f9cf653"

curl -X POST \
  https://apps.sematext.com/users-web/api/v3/savedQueries \
  -H "Authorization: apiKey ${API_KEY}" \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -d @alert.json