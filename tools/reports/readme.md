# Reporting on failed SMS Messages

To report on undeliverable SMS messages you can update query.json with the appropriate date range and then run failedSmsReport.sh
 
To get the impacted businesses you can pass the phone numbers from this csv into the following query. 

```
select business_id
from workers
where phone_number in ( ? );
```