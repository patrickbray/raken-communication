#!/bin/bash
set -x -e 
http -a apiKey:${SEMATEXT_API_KEY:=88c61d4c-8d65-4a15-bbd5-30977f9cf653} logsene-receiver.sematext.com/4e5b7d34-cf2c-45cc-a5d8-19fabe01aacc/_search < query.json | jq '.hits.hits[]._source' | jq -r '[.to,.traceId,.twilio_error_code] | @csv'